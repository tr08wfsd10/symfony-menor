<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\UsuarioController;
/**
 * 
 */
class MenorController extends AbstractController
{
	/**
 	 * 	@Route("/edad/{edad}", methods={"GET"})
 	 */
	public function consultar($edad)
	{
		$usuario=new UsuarioController($edad);
		return $this->render(
			'menor.html.twig',
			[
				"usuario" => $usuario
			]
		);
	}
}